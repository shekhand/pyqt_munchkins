from PyQt5.QtWidgets import QWidget, QApplication, QHBoxLayout
from PyQt5 import QtGui, QtCore
from player_widget import PlayerWidget

class MainWindow(QWidget):
    keyPressed = QtCore.pyqtSignal(int)

    def __init__(self):
        super().__init__()

        self.layout = QHBoxLayout()
        self.players = [
            PlayerWidget(player_name='Player 1'),
            PlayerWidget(player_name='Player 2'),
            PlayerWidget(player_name='Player 3')
        ]

        self.chosen_player = 0
        self.chosen_parameter = 0

        # Put border on first player level
        self.players[self.chosen_player].level_frame.setFrameShape(1)

        self.layout.addStretch(1)
        for pl in self.players:
            self.layout.addWidget(pl)
        self.layout.addStretch(1)
        self.setLayout(self.layout)

        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.keyPressed.connect(self.on_key)

    def keyPressEvent(self, event):
        super(MainWindow, self).keyPressEvent(event)
        self.keyPressed.emit(event.key())

    def on_key(self, key):
        if key == QtCore.Qt.Key_Up:
            if self.chosen_parameter == 0:
                self.players[self.chosen_player].level_up_slot()
            else:
                self.players[self.chosen_player].gear_up_slot()

        elif key == QtCore.Qt.Key_Down:
            if self.chosen_parameter == 0:
                self.players[self.chosen_player].level_down_slot()
            else:
                self.players[self.chosen_player].gear_down_slot()

        elif key == QtCore.Qt.Key_Left:
            self.disable_frame()
            if self.chosen_parameter == 0:
                self.chosen_player = self.chosen_player - 1 if self.chosen_player > 0 else len(self.players) - 1
                self.chosen_parameter = 1
            else:
                self.chosen_parameter = 0
            self.enable_frame()

        elif key == QtCore.Qt.Key_Right:
            self.disable_frame()
            if self.chosen_parameter == 1:
                self.chosen_player = (self.chosen_player + 1) % len(self.players)
                self.chosen_parameter = 0
            else:
                self.chosen_parameter = 1
            self.enable_frame()

    def disable_frame(self):
        if self.chosen_parameter == 0:
            self.players[self.chosen_player].level_frame.setFrameShape(0)
        else:
            self.players[self.chosen_player].gear_frame.setFrameShape(0)

    def enable_frame(self):
        if self.chosen_parameter == 0:
            self.players[self.chosen_player].level_frame.setFrameShape(1)
        else:
            self.players[self.chosen_player].gear_frame.setFrameShape(1)


if __name__=='__main__':
    app = QApplication([])

    mw = MainWindow()
    mw.show()

    app.exec_()
