class Player:
    def __init__(self, name):
        self.__name = name
        self.__gear = 0
        self.__level = 1
        self.__sex = 0

    @property
    def gear(self):
        return self.__gear

    @property
    def level(self):
        return self.__level

    @property
    def name(self):
        return self.__name

    @property
    def strength(self):
        return self.__level + self.__gear

    def level_up(self):
        if self.__level < 10:
            self.__level += 1

    def level_down(self):
        if self.__level > 1:
            self.__level -= 1

    def gear_up(self):
        self.__gear += 1

    def gear_down(self):
        if self.__gear > 0:
            self.__gear -= 1

    def reset(self):
        self.__gear = 0
        self.__level = 1

    def change_sex(self):
        self.__sex = self.__sex ^ 1
        return self.__sex
