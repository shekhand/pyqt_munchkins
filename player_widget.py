from PyQt5 import QtWidgets, uic
from PyQt5 import QtGui
import resources
from player import Player

class PlayerWidget(QtWidgets.QWidget):
    def __init__(self, parent=None, player_name=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi('ui/player_widget.ui', self)

        if not player_name:
            self.__player = Player('Player name')
        else:
            self.__player = Player(player_name)

        self.update_labels()

        self.player_name_label.setText(self.__player.name)

        self.level_up.clicked.connect(self.level_up_slot)
        self.level_down.clicked.connect(self.level_down_slot)
        self.gear_up.clicked.connect(self.gear_up_slot)
        self.gear_down.clicked.connect(self.gear_down_slot)

        self.sex_icon_button.clicked.connect(self.change_sex_slot)
        self.reset_button.clicked.connect(self.reset_slot)

    def level_up_slot(self):
        self.__player.level_up()
        self.update_labels()

    def level_down_slot(self):
        self.__player.level_down()
        self.update_labels()

    def gear_up_slot(self):
        self.__player.gear_up()
        self.update_labels()

    def gear_down_slot(self):
        self.__player.gear_down()
        self.update_labels()

    def reset_slot(self):
        self.__player.reset()
        self.update_labels()

    def change_sex_slot(self):
        sex = self.__player.change_sex()
        if sex == 0:
            self.sex_icon_button.setStyleSheet('border-image: url(:/icon/male.png);')
        else:
            self.sex_icon_button.setStyleSheet('border-image: url(:/icon/female.png);')

    def update_labels(self):
        self.level_counter_label.setText(str(self.__player.level))
        self.gear_counter_label.setText(str(self.__player.gear))
        self.strength_counter_label.setText(str(self.__player.strength))

if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    window = PlayerWidget()
    window.show()
    app.exec_()
